#ifndef __IFMO_DISTRIBUTED_CLASS_PROCESS__H
#define __IFMO_DISTRIBUTED_CLASS_PROCESS__H

#include <sys/types.h>
#include "ipc.h"
#include "banking.h"

typedef struct {
    local_id id;
    pid_t in;
    pid_t out;
} ProcessPipe;

typedef struct {
	local_id id;
    local_id pipes_count;
    BalanceHistory balance_history;
    ProcessPipe* pipes;
} Process;

#endif // __IFMO_DISTRIBUTED_CLASS_PROCESS__H
