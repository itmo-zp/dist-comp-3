#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "worker.h"
#include "ipc.h"
#include "log.h"
#include "banking.h"
#include "process.h"
#include "skipped_messages.h"
#include "lamport.h"
#include "ipc_lamport.h"

static void wait_workers(Process* worker, MessageType type, SkippedMessages* skipped_messages);

static void prepare(Process* worker);
static void do_work(Process* worker, SkippedMessages* skipped_messages);
static void do_transfer(Process* worker, Message* message);
static void finish(Process* worker, SkippedMessages* skipped_messages);

static void fill_history(BalanceHistory* history, timestamp_t last_time);

static void wait_workers(Process* worker, MessageType type, SkippedMessages* skipped_messages) {
    int wait_workers_count = worker->pipes_count - 1;
    int received_workers_count = 0;
    int received_workers[wait_workers_count];
    Message received_message;

    for (int i = 0; i < wait_workers_count; i++) {
        received_workers[i] = 0;
    }

    local_id parent_index = 0;
    for (local_id i = 0; i < worker->pipes_count; i++) {
        if (worker->pipes[i].id == PARENT_ID) {
            parent_index = i;
        }
    }

    if (skipped_messages != NULL) {
        for (int i = 0; i < skipped_messages->messages_count; i++) {
            SkippedMessage skipped_message = skipped_messages->messages[i];
            if (skipped_message.message.s_header.s_type == type) {
                for (int worker_index = 0; worker_index < worker->pipes_count; worker_index++) {
                    if (worker_index == parent_index) continue;
                    if (worker->pipes[worker_index].id == skipped_message.from) {
                        received_workers[worker_index > parent_index ? worker_index - 1 : worker_index] = 1;
                        received_workers_count++;
                        break;
                    }
                }
            }
        }
    }

    while (received_workers_count < wait_workers_count) {
        for (int i = 0; i < worker->pipes_count; i++) {
            if (i == parent_index) continue;
            int worker_index = i > parent_index ? i - 1 : i;
            if (
                    !received_workers[worker_index]
                    && receive_lamport(worker, worker->pipes[i].id, &received_message) == 0
                    && received_message.s_header.s_type == type
            ) {
                received_workers[worker_index] = 1;
                received_workers_count++;
            }
        }
    }
}

static void prepare(Process* worker) {
    Message message = {
            .s_header = {
                    .s_type = STARTED,
                    .s_magic = MESSAGE_MAGIC,
                    .s_payload_len = 48,
                    .s_local_time = get_lamport_time()
            }
    };
    send_lamport_multicast(worker, &message);

    wait_workers(worker, STARTED, NULL);
}

static void fill_history(BalanceHistory* history, timestamp_t last_time) {
    int states_count = last_time - (history->s_history_len - 1);
    balance_t states_balance = history->s_history[history->s_history_len - 1].s_balance;
    balance_t pending_balance = history->s_history[history->s_history_len - 1].s_balance_pending_in;
    for (timestamp_t i = history->s_history_len; i <= last_time; i++) {
        history->s_history[i] = (BalanceState) {
            .s_balance = states_balance,
            .s_time = i,
            .s_balance_pending_in = pending_balance
        };
    }
    history->s_history_len += states_count;
}

static void new_balance_state(BalanceHistory* history, timestamp_t time, balance_t balance_diff, balance_t balance_pending_in_diff) {
    fill_history(history, time);
    history->s_history[time].s_balance += balance_diff;
    history->s_history[time].s_balance_pending_in += balance_pending_in_diff;
    history->s_history[time].s_time = time;
}

static void do_transfer(Process* worker, Message* message) {
    TransferOrder* order = (TransferOrder*) message->s_payload;

    if (order->s_src == worker->id) {
        new_balance_state(&worker->balance_history, message->s_header.s_local_time, -order->s_amount, order->s_amount);
        log_transfer_out(order->s_src, order->s_dst, order->s_amount);

        send_lamport(worker, order->s_dst, message);

        Message received_message;
        while(receive_lamport(worker, order->s_dst, &received_message) != 0 || received_message.s_header.s_type != ACK);
        new_balance_state(&worker->balance_history, received_message.s_header.s_local_time, 0, -order->s_amount);
    } else if (order->s_dst == worker->id) {
        Message ack_message = {
                .s_header = {
                        .s_type = ACK,
                        .s_magic = MESSAGE_MAGIC,
                        .s_payload_len = 0,
                        .s_local_time = get_lamport_time()
                }
        };
        send_lamport(worker, PARENT_ID, &ack_message);
        // Not lamport - consider as multicast
        send((void*) worker, order->s_src, &ack_message);

        new_balance_state(&worker->balance_history, get_lamport_time(), order->s_amount, 0);
        log_transfer_in(order->s_src, order->s_dst, order->s_amount);
    } else {
        return;
    }
}

static void do_work(Process* worker, SkippedMessages* skipped_messages) {
    SkippedMessage skipped_message;
    Message received_message;
    for(;;) {
        for (int i = 0; i < worker->pipes_count; i++) {
            if (receive_lamport(worker, worker->pipes[i].id, &received_message) == 0) {
                switch (received_message.s_header.s_type) {
                    case TRANSFER:
                        do_transfer(worker, &received_message);
                        break;
                    case STOP:
                        return;
                    default:
                        skipped_message = (SkippedMessage) {
                                .from = worker->pipes[i].id,
                                .message = received_message
                        };
                        skip_message(skipped_messages, &skipped_message);
                        continue;
                }
            }
        }
    }

}

static void finish(Process* worker, SkippedMessages* skipped_messages) {
    Message message = {
            .s_header = {
                    .s_type = DONE,
                    .s_magic = MESSAGE_MAGIC,
                    .s_payload_len = 28,
                    .s_local_time = 0
            }
    };
    send_lamport_multicast(worker, &message);

    wait_workers(worker, DONE, skipped_messages);

    fill_history(&worker->balance_history, get_lamport_time());
    Message history_message = {
            .s_header = {
                    .s_type = BALANCE_HISTORY,
                    .s_magic = MESSAGE_MAGIC,
                    .s_payload_len = sizeof(BalanceHistory),
                    .s_local_time = get_lamport_time(),
            }
    };
    memcpy(&history_message.s_payload, &worker->balance_history, sizeof(BalanceHistory));
    send_lamport(worker, PARENT_ID, &history_message);
}

int start_worker(Process* process) {
    SkippedMessages skippedMessages = {
            .messages = NULL,
            .messages_count = 0
    };

    log_started(process);
    prepare(process);
    log_received_all_started(process);
    do_work(process, &skippedMessages);
    log_done(process);
    finish(process, &skippedMessages);
    log_received_all_done(process);

    free(skippedMessages.messages);
    return 0;
}
