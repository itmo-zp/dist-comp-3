#include "ipc_lamport.h"
#include "lamport.h"

int receive_lamport(Process* self, local_id from, Message* msg) {
    int err = receive((void*) self, from, msg);
    if (err) return err;

    timestamp_t message_time = msg->s_header.s_local_time;
    if (get_lamport_time() < message_time) {
        set_lamport_time(message_time);
    }
    increment_lamport_time();
    return 0;
}

int send_lamport(Process* self, local_id dst, Message* msg) {
    increment_lamport_time();
    msg->s_header.s_local_time = get_lamport_time();
    return send((void*) self, dst, msg);
}

int send_lamport_multicast(Process* self, Message* msg) {
    increment_lamport_time();
    msg->s_header.s_local_time = get_lamport_time();
    for (local_id i = 0; i < self->pipes_count; i++) {
        int err = send(self, self->pipes[i].id, msg);
        if (err) {
            return err;
        }
    }

    return 0;
}
