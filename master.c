#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdbool.h>
#include "master.h"
#include "ipc.h"
#include "log.h"
#include "banking.h"
#include "process.h"
#include "lamport.h"
#include "ipc_lamport.h"

static void wait_workers_messages(Process* master, MessageType type, Message* messages);

static void prepare(Process* master);
static void do_work(Process* master);
static void finish(Process* master);

static void wait_workers_messages(Process* master, MessageType type, Message* messages) {
    int wait_workers_count = master->pipes_count;
    int received_workers_count = 0;
    int received_workers[wait_workers_count];

    for (int i = 0; i < wait_workers_count; i++) {
        received_workers[i] = 0;
    }

    while (received_workers_count < wait_workers_count) {
        for (int i = 0; i < wait_workers_count; i++) {
            if (
                    !received_workers[i]
                    && receive_lamport(master, master->pipes[i].id, &messages[i]) == 0
                    && messages[i].s_header.s_type == type
            ) {
                received_workers[i] = 1;
                received_workers_count++;
            }
        }
    }
}

static void prepare(Process* master) {
    Message workers_messages[master->pipes_count];
    wait_workers_messages(master, STARTED, workers_messages);
}

static void do_work(Process* master) {
    bank_robbery((void*) master, master->pipes_count);
}

static void finish(Process* master) {
    Message stop_msg = {
            .s_header = {
                    .s_type = STOP,
                    .s_magic = MESSAGE_MAGIC,
                    .s_payload_len = 0,
                    .s_local_time = get_lamport_time()
            }
    };
    send_lamport_multicast(master, &stop_msg);

    Message workers_messages[master->pipes_count];
    wait_workers_messages(master, BALANCE_HISTORY, workers_messages);

    AllHistory history = {
            .s_history_len = (uint8_t)master->pipes_count
    };
    for (int i = 0; i < master->pipes_count; i++) {
        history.s_history[i] = *(BalanceHistory*) workers_messages[i].s_payload;
    }

    print_history(&history);
}

int start_master(Process* master) {
    prepare(master);
    do_work(master);
    finish(master);
    return 0;
}

void transfer(void* parent_data, local_id src, local_id dst, balance_t amount) {
    TransferOrder order = {
            .s_src = src,
            .s_dst = dst,
            .s_amount = amount
    };
    Message transfer_msg = {
            .s_header = {
                    .s_type = TRANSFER,
                    .s_magic = MESSAGE_MAGIC,
                    .s_payload_len = sizeof(TransferOrder),
                    .s_local_time = get_lamport_time()
            }
    };
    memcpy(&transfer_msg.s_payload, &order, sizeof(order));
    send_lamport(parent_data, src, &transfer_msg);

    Message received_msg;
    while(receive_lamport(parent_data, dst, &received_msg) != 0 || received_msg.s_header.s_type != ACK);
}

