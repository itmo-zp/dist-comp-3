#ifndef __IFMO_DISTRIBUTED_CLASS_WORKER__H
#define __IFMO_DISTRIBUTED_CLASS_WORKER__H

#include "process.h"

int start_worker(Process *process);

#endif // __IFMO_DISTRIBUTED_CLASS_WORKER__H
