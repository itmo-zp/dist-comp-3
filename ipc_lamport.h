#ifndef __IFMO_DISTRIBUTED_CLASS_IPC_LAMPORT__H
#define __IFMO_DISTRIBUTED_CLASS_IPC_LAMPORT__H

#include "ipc.h"
#include "process.h"

int receive_lamport(Process* self, local_id from, Message* msg);

int send_lamport(Process* self, local_id dst, Message* msg);

int send_lamport_multicast(Process* self, Message* msg);

#endif //__IFMO_DISTRIBUTED_CLASS_IPC_LAMPORT__H
