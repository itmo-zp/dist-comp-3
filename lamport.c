#include "lamport.h"

timestamp_t lamport_time = 0;

timestamp_t get_lamport_time() {
    return lamport_time;
}

void set_lamport_time(timestamp_t new_time) {
    lamport_time = new_time;
}

void increment_lamport_time() {
    lamport_time++;
}
